import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.css';
import Navbar from './components/Navbar/Navbar';
import Profile from './views/Profile';
import Startup from './views/Startup';
import Translation from './views/Translation';
import 'bootstrap/dist/css/bootstrap.min.css';
import "animate.css";

function App() {
  return (
    <BrowserRouter>
      <div className="App bg-dark text-light h-100">
      <Navbar />
      <Routes>
          <Route path="/" element={ <Startup /> } />
          <Route path="/translations" element={ <Translation /> } />
          <Route path="/profile" element={ <Profile/> } />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
