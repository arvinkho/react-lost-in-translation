import { useSelector } from "react-redux"
import { Navigate } from "react-router-dom";


const withAuth = Component => props => {
    const currentUser = useSelector((state) => state.user);
    if (currentUser.username !== "") {
        return <Component {...props} />
    } else {
        return <Navigate to="/" />;
    };
};

export default withAuth;