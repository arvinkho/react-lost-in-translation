
import {  Container, Nav, NavbarBrand, NavLink } from "react-bootstrap";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux"
import { Link, useLocation } from "react-router-dom";
import { attemptSignOut } from "../../features/User/userState";
import { actionResetTranslation } from "../Translation/translationState";


const Navbar = () => {
    const currentUser = useSelector((state) => state.user);
    const dispatch = useDispatch();
    const location = useLocation()

    const onSignOut = () => {
        dispatch(attemptSignOut());
        dispatch(actionResetTranslation());
    }

    if (location.pathname !== "/translations") {
        dispatch(actionResetTranslation());
    }

    return (
   
      <nav className="navbar navbar-expand-lg navbar-light bg-warning mb-4">
        <Container fluid>
            <NavbarBrand>
                {
                    location.pathname === "/" &&
                    <img src="img/Logo-Hello.png" width="80" height="80" className="d-inline-block align-center" alt=""/>
                }
                {
                    location.pathname !== "/" &&
                    <img src="img/Logo.png" width="80" height="80" className="d-inline-block align-center" alt=""/>
                }
                Sign language translator
            </NavbarBrand>

            <Nav className="me-auto">
                {currentUser.username !== "" && 
                    <>
                    <Link className="nav-link" to="/profile">Profile</Link>
                    <Link className="nav-link" to="/translations">Translations</Link>
                    </>
                }   
            </Nav>

            <Nav>
            {currentUser.username !== "" && 
            <>
                <Link className="nav-link text-primary" to="/profile">
                {currentUser.username}
                </Link>
                <NavLink className="nav-link" onClick={onSignOut}>
                    Sign out
                </NavLink>
            </>
            }   
            </Nav>
        </Container>
        </nav>
       
    );
}

export default Navbar;

