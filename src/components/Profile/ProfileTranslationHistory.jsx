import ProfileTranslationHistoryItem from "./ProfileTranslationHistoryItem";


const ProfileTranslationHistory = ({translations}) => {


    const translationList = translations.slice(-10).map(
        (translation, index) => <ProfileTranslationHistoryItem 
                                    key={index}
                                    translation={translation} />
    )
    return (
        <section>
            <h5>Your translation history</h5>

            { translationList.length === 0 && <p>Your translation history is empty.</p>}

            <ul>
                {translationList}
            </ul>
        </section>
    )
};

export default ProfileTranslationHistory;