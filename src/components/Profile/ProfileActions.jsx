import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { actionClearHistory } from "../../features/User/userState";


const ProfileActions = () => {

    const currentUser = useSelector((state) => state.user);
    const { handleSubmit } = useForm();
    const dispatch = useDispatch();

    const onSubmit = () => {
        if(!window.confirm("Are you sure?\nThis can't be undone!")){
            return;
        }
        dispatch(actionClearHistory(currentUser));
    }

    return (
        <>
        <form onSubmit={handleSubmit(onSubmit)}>
            <button type="submit" className="btn btn-outline-primary">Clear history</button>
        </form>
        </>
    )
}

export default ProfileActions;