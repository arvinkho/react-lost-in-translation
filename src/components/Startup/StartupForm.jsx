import { useEffect, useRef } from "react";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { STORAGE_KEY_USER } from "../../const/storageKeys";
import { actionAttemptSignIn, signIn } from "../../features/User/userState";
import { storageRead } from "../../utils/storage";

const usernameConfig = {
    required: true,
    minLength: 3
}

const StartupForm = () => {

    const { register, handleSubmit, formState:{ errors } } = useForm()
    const navigate = useNavigate();
    const currentUser = useSelector((state) => state.user);
    const dispatch = useDispatch();
    const loading = useRef(false);

    

    useEffect(() => {
        if (storageRead(STORAGE_KEY_USER) !== null) {
            dispatch(signIn(storageRead(STORAGE_KEY_USER)));
        }
        if (currentUser.username !== "") {
            loading.current = false;
            navigate("translations")
        }
    }, [currentUser, navigate]);

    const errorMessage = (() => {
        if (!errors.username) {
            return null;
        }

        if (errors.username.type === "required") {
            return <span>Please enter your username!</span>
        }

        if (errors.username.type === "minLength") {
            return <span>Username too short! (Min. 3 characters)</span>
        }
    })();

    const onSubmit = ({username}) => {
        loading.current = true;
        dispatch(actionAttemptSignIn(username));
    }



    return (
        <>
        <h2 className="animate__animated animate__bounceInUp">What's your name?</h2>
        <form onSubmit={handleSubmit(onSubmit)} className="animate__animated animate__bounceInUp">
            <fieldset>
                <label htmlFor="username">Username: </label>
                <div className="input-group mb-3">
                <input 
                    type="text" 
                    placeholder="What's your name?"
                    {...register("username", usernameConfig)}
                    className="form-control">
                </input>
                
                <button type="submit" className="btn btn-outline-primary btn-lg">Login</button>
                </div>
                {errorMessage}
                {
                    loading.current && <p>Loading</p> 
                }
                {
                    currentUser.username !== "" && currentUser.username
                }
            </fieldset>
        </form>
        </>
    );
};

export default StartupForm;