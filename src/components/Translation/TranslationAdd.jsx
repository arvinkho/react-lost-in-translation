import "../../views/Translation.css"

const TranslationAdd = ({letterObj}) => {

    

    return (
        <>
        {
            letterObj.name !== " " &&
        <aside>
            
            <img src={letterObj.image} alt={letterObj.name} className="rounded mx-auto d-block" width="100%"/>
            <section className="Translation text-dark"><b>{letterObj.name.toUpperCase()}</b></section>
        </aside>
        }   

        {
            letterObj.name === " " &&
            <section className="mx-2"></section>
        }
        </>
    );
};

export default TranslationAdd;