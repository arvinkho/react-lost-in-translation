// Action Types
// Get array with each letter render word
// Render Images
// Add translation to history?

export const ACTION_TRANSLATION_UPDATE_WORD = "[translation] UPDATE_WORD";
export const ACTION_TRANSLATION_UPDATE_LETTER_ARRAY = "[translation] UPDATE_LETTER_ARRAY";
export const ACTION_TRANSLATION_RENDER_IMAGES = "[translation] RENDER_IMAGES";
export const ACTION_TRANSLATION_RESET = "[translation] RESET";


// Actions - NO LOGIC

export const actionUpdateWord = (word) => ({
    type: ACTION_TRANSLATION_UPDATE_WORD,
    payload: word,
})

export const actionUpdateLetterArray = (letterArray) => ({
    type: ACTION_TRANSLATION_UPDATE_LETTER_ARRAY,
    payload: letterArray,
});

export const actionResetTranslation = () => ({
    type: ACTION_TRANSLATION_RESET
})


const initTranslationState = {
    word: "",
    letterArray: [],
}


export const translationReducer = (state = initTranslationState, action) => {
    switch (action.type) {

        case ACTION_TRANSLATION_UPDATE_WORD:
            return {
                ...state,
                word: action.payload
            }

        case ACTION_TRANSLATION_UPDATE_LETTER_ARRAY:
            
            return {
                ...state,
                letterArray: action.payload
            };
        
        case ACTION_TRANSLATION_RESET:
            return initTranslationState;
    
        default:

            return state;
    }
}

export const translationMiddleware = ({dispatch}) => (next) => async (action) => {
    next(action);
    
}