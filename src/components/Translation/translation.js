import { createHeaders } from "../../api";


const API_URL = process.env.REACT_APP_API_URL;
let SIGNS = []

/**
 * Attempts to add a translation to the history in the API
 * by sending a HTTP request (PATCH).
 * If successful, returns a new user object with cleared history.
 * If unsuccessful, return an error message.
 * @param {*} user user object to update
 * @param {*} translation translation to add
 * @returns [error message, new user object]
 */
export const translationAdd = async (user, translation) => {
    try {
        const response = await fetch(`${API_URL}/${user.id}`, {
            method: "PATCH",
            headers: createHeaders(),
            body: JSON.stringify({
                translations: [...user.translations, translation]
            })
        });

        if (!response.ok) {
            throw new Error("Could not update the translation");
        }
        const result = await response.json();
        return [null, result]
    } catch (error) {
        return [error.message, null];
    };
}

/**
 * Attempts to clear the translation history in the API by
 * sending a HTTP request (PATCH).
 * If successful, returns a new user object with cleared history.
 * If unsuccessful, return an error message.
 * @param {object} user user object to update
 * @returns [error message, new user object]
 */
export const translationClearHistory = async (user) => {
    try {
        const response = await fetch(`${API_URL}/${user.id}`, {
            method: "PATCH",
            headers: createHeaders(),
            body: JSON.stringify({
                translations: []
            })
        });
        if (!response.ok) {
            throw new Error("Could not update translations");
        };
        const result = await response.json();
        return [null, result]
    } catch (error) {
        return [error.message, null];
    }
}

/**
 * Creates an array containing each letter of the input string
 * as objects. 
 * @param {string} translateString string to translate
 * @returns array of letter objects
 */
export const getTranslationLetters = (translateString) => {
    let renderLetters = [];
    for (let i = 0; i < translateString.length; i++){
        let letter = translateString[i].toLowerCase();
        let letterObj = {
            name: " ",
            id: 0
        }
        if (letter !== " ") {
            letterObj = SIGNS.find(obj => obj.name === letter);
        } 
        renderLetters.push(letterObj);
    };
    return renderLetters;
}

/**
 * Generates an array containing 26 objects. Each object
 * is a letter, with properties 
 * "name": the letter,
 * "id": the position in the alphabet,
 * "image": the path of the image corresponding to the letter.
 */
const generateSignsObject = (() => {
    if (SIGNS.length === 0) {
        for (let i = 0; i < 26; i++) {
            SIGNS.push({
                id: i + 1,
                name: String.fromCharCode(97 + i),
                image: "img/" + String.fromCharCode(97 + i) + ".png"
            })
        }
    }
})();