
import { Container } from "react-bootstrap";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { getTranslationLetters } from "../components/Translation/translation";
import TranslationAdd from "../components/Translation/TranslationAdd";
import { actionUpdateLetterArray, actionUpdateWord } from "../components/Translation/translationState";
import { actionAddToHistory } from "../features/User/userState";
import withAuth from "../hoc/withAuth";

// Config params for the input form.
const translatorConfig = {
    required: true,
    maxLength: 40,
    pattern: /^[a-zA-Z ]+$/,
}


const Translation = () => {
    const { register, handleSubmit, watch, formState: { errors } } = useForm();
    const currentUser = useSelector((state) => state.user);
    const currentTranslation = useSelector((state) => state.translation);
    const dispatch = useDispatch();

    const onSubmit = data => {
        
        dispatch(actionUpdateWord(data.translateString));
        dispatch(actionUpdateLetterArray(getTranslationLetters(data.translateString)));
        dispatch(actionAddToHistory(currentUser, data.translateString));
    }
    
    const errorMessage = (() => {
        if (!errors.translateString) {
            return null;
        }
        if (errors.translateString.type === "maxLength") {
            return <span>Max characters: 40</span>
        }
        if (errors.translateString.type === "pattern") {
            return <span>Please only use letters from A-Z</span>
        }
    })();

    return (
        <Container>
        <div className=" animate__animated animate__bounceInUp">
        <h2>Translations</h2>
        { watch().translateString && <p>Remaining characters: {translatorConfig.maxLength - watch().translateString.length} </p>}
            { !watch().translateString && <p>Remaining characters: {translatorConfig.maxLength} </p>}
            {errorMessage}
        <form onSubmit={handleSubmit(onSubmit)}>
            <fieldset>
            <div className="input-group mb-3">
            <input 
                type="text" 
                placeholder="Write something to translate"
                { ...register("translateString", translatorConfig)}
                className="form-control"
            />
            <button type="submit" className="btn btn-outline-primary btn-lg">Translate</button>
            </div>
            
            </fieldset>
        </form>
        <div key={currentTranslation.word} className="d-flex justify-content-center animate__animated animate__flipInX border rounded shadow bg-light mt-4">
        {currentTranslation.letterArray.map( (letter, index)=> {
            return  <TranslationAdd
                        key={index}
                        letterObj={letter} />
        })}
        </div>
        </div>
        </Container>
    );
}

export default withAuth(Translation);