import StartupForm from "../components/Startup/StartupForm";


const Startup = () => {
    return (
        <div className="container">
        <StartupForm />
        </div>
    );
}

export default Startup;