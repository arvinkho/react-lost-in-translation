import { useSelector } from "react-redux";
import ProfileActions from "../components/Profile/ProfileActions";
import ProfileHeader from "../components/Profile/ProfileHeader";
import ProfileTranslationHistory from "../components/Profile/ProfileTranslationHistory";
import withAuth from "../hoc/withAuth";


const Profile = () => {

    const currentUser = useSelector((state) => state.user);


    return (
        <>
        <div className="container d-flex justify-content-between">
        {/* <h3 >Profile</h3> */}
        <ProfileHeader username= {currentUser.username} />
        <ProfileActions />
        </div>
        <div className="container d-flex justify-content-start">
        <ProfileTranslationHistory translations={currentUser.translations}/>
        </div>
        </>
    );
}

export default withAuth(Profile);