import { applyMiddleware, combineReducers, createStore } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import { translationReducer } from "./components/Translation/translationState";
import { userMiddleware, userReducer } from "./features/User/userState";



const rootReducers = combineReducers({
    user: userReducer,
    translation: translationReducer
});

export default createStore(rootReducers, composeWithDevTools(applyMiddleware(userMiddleware)));