import { createHeaders } from "../../api";

const API_URL = process.env.REACT_APP_API_URL;

const checkForUser = async (username) => {
    try {
        const response = await fetch(`${API_URL}?username=${username}`);
        if (!response.ok) {
            throw new Error("Could not complete request");
        }
        const user = await response.json();
        return [null, user];
        
    } catch (error) {
        return [error.message, null];
    };
};

const createUser = async (username) => {
    try {
        const response = await fetch(API_URL, {
            method: "POST", // Create a resource
            headers: createHeaders(),
            body: JSON.stringify({
                username,
                translations: []
            })
        });
        if (!response.ok) {
            throw new Error("Could not create user with username " + username);
        }
        const user = await response.json();
        return [null, user];
    } catch (error) {
        return [error.message, []];
    };
};

export const attemptLogin = async (username) => {
    try {
        const [checkError, user] = await checkForUser(username);
        if (checkError !== null) {
            return [checkError, null];
        };
        
        if (user.length > 0) {
            return [null, user.pop()];
        };
        return await createUser(username);
    } catch (error) {
        return [error.message, null];
    }
}