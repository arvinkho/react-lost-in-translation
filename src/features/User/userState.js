// Action types
//Logged in
//Clear history
//Add to history

import { translationAdd, translationClearHistory } from "../../components/Translation/translation";
import { STORAGE_KEY_USER } from "../../const/storageKeys";
import { storageDelete, storageSave } from "../../utils/storage";
import { attemptLogin } from "./user";


export const ACTION_USER_ATTEMPT_SIGN_IN = "[user] ATTEMPT_SIGN_IN"
export const ACTION_USER_SIGN_IN = "[user] SIGN_IN";
export const ACTION_USER_SIGN_OUT = "[user] SIGN_OUT";
export const ACTION_USER_ATTEMPT_SIGN_OUT = "[user] ATTEMPT_SIGN_OUT";
export const ACTION_USER_CLEAR_HISTORY = "[user] CLEAR_HISTORY";
export const ACTION_USER_ADD_TO_HISTORY = "[user] ADD_TO_HISTORY";
export const ACTION_USER_ATTEMPT_ADD_TO_HISTORY = "[user] ATTEMPT_ADD_TO_HISTORY";
export const ACTION_USER_UPDATE_USER = "[user] UPDATE_USER";


// Actions - NO LOGIC
export const actionAttemptSignIn = (username) => ({
    type: ACTION_USER_ATTEMPT_SIGN_IN,
    payload: username
})

export const signIn = (user) => ({
    type: ACTION_USER_SIGN_IN,
    user
});

export const signOut = () => ({
    type: ACTION_USER_SIGN_OUT
});

export const attemptSignOut = () => ({
    type: ACTION_USER_ATTEMPT_SIGN_OUT
});

export const actionClearHistory = (user) => ({
    type: ACTION_USER_CLEAR_HISTORY,
    user
});

export const actionAddToHistory = (user, translation) => ({
    type: ACTION_USER_ADD_TO_HISTORY,
    user,
    translation
})

export const actionUpdateUser = (user) => ({
    type: ACTION_USER_UPDATE_USER,
    user
})


// Reducers
// Init state as object with empty props?!?!?!

const initUserState = {
    username: "",
    translations: []

}

export const userReducer = (state = initUserState, action) => {

    switch (action.type) {

        case ACTION_USER_SIGN_IN:
            return action.user;

        case ACTION_USER_SIGN_OUT:
            return initUserState;

        case ACTION_USER_UPDATE_USER:
            return {
                ...state,
                user: action.user
            };
    
        default:
            return state;
    }
}


export const userMiddleware = ({ dispatch }) => (next) => async (action) => {
    
    next(action);

    if (action.type === ACTION_USER_ATTEMPT_SIGN_OUT) {
        storageDelete(STORAGE_KEY_USER);
        dispatch(signOut());
    }

    let [error, updatedUser] = [null, null];
    if (action.type === ACTION_USER_ATTEMPT_SIGN_IN){
        [error, updatedUser] = await attemptLogin(action.payload);
    }

    if (action.type === ACTION_USER_ADD_TO_HISTORY) {
        [error, updatedUser] = await translationAdd(action.user, action.translation);
    }

    if (action.type === ACTION_USER_CLEAR_HISTORY) {
        [error, updatedUser] = await translationClearHistory(action.user);
    }
    if (error !== null) {
        console.log(error.message);
    };
    if (updatedUser !== null) {
        storageSave(STORAGE_KEY_USER, updatedUser);
        dispatch(signIn(updatedUser));
    }
}

